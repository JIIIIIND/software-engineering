﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PortfolioManagement
{
    class Card : Panel
    {
        private ElementPanel elementPanel;
        private CheckBox check;
        private TextBox name;
        private Panel itemPanel;
        private List<Item> itemList;
        private Memo memo;
        private int index;
        private Point prePosition;
        private Point mousePosition;
        private bool mouseClick = false;

        private static int defaultCardWidth = 200;
        private static int defaultCardHeight = 355;
        private static int defaultCardNameHeight = 23;
        private static int defaultButtonHeight = 25;

        private Card(int num, String cardName)
        {
            InitializeComponent(num, cardName);
        }

        public Card(CardData cardData)
        {
            InitializeComponent(cardData.CardIndex, cardData.CardName);
        }

        private void InitializeComponent(int num, String cardName)
        {
            Panel cardPanel = new System.Windows.Forms.Panel();
            check = new System.Windows.Forms.CheckBox();
            name = new System.Windows.Forms.TextBox();
            index = num;

            itemPanel= new Panel();
            itemList = new List<Item>();

            Panel buttonPanel = new Panel();
            memo = new Memo(cardPanel);

            //this.Location = new System.Drawing.Point(10 + (defaultCardWidth + 10) * num, 10);
            int test = DataSet.Instance.FindCardData(cardName, num).CardIndex;
            this.Location = new System.Drawing.Point(10 + (defaultCardWidth + 10) * DataSet.Instance.FindCardData(cardName, num).CardIndex, 10);
            this.Size = new System.Drawing.Size(defaultCardWidth, defaultCardHeight);
            this.BackColor = System.Drawing.Color.White;
            this.BorderStyle = BorderStyle.FixedSingle;     

            //카드 이름 설정
            cardPanel.Dock = DockStyle.Top;
            cardPanel.Location = new System.Drawing.Point(0, 0);
            //cardPanel.Text = cardName;
            cardPanel.Text = DataSet.Instance.FindCardData(cardName, num).CardName;
            cardPanel.Size = new System.Drawing.Size(defaultCardWidth, defaultCardNameHeight);
            cardPanel.Padding = new Padding(10, 0, 0, 0);
            cardPanel.ContextMenu = memo.GetContextMenu();

            check.Size = new System.Drawing.Size(13, 13);
            check.Location = new System.Drawing.Point(5, 4);
            check.CheckedChanged += CardName_CheckedChanged;

            //name.Text = cardName;
            name.Text = DataSet.Instance.FindCardData(cardName, num).CardName;
            name.Location = new System.Drawing.Point(check.Width + 10, 5);
            name.Size = new System.Drawing.Size(cardPanel.Width - check.Width + 10, name.Font.Height + 4);
            name.BackColor = System.Drawing.Color.White;
            name.ForeColor = System.Drawing.Color.Black;
            name.BorderStyle = BorderStyle.None;
            name.Enabled = false;
            name.ReadOnly = true;

            cardPanel.Controls.Add(check);
            cardPanel.Controls.Add(name);

            //Button Panel 설정
            buttonPanel.Dock = DockStyle.Top;
            buttonPanel.Size = new System.Drawing.Size(defaultCardWidth, defaultButtonHeight);

            SetItemlButton(buttonPanel, "MakeItem", "생성", 0);
            SetItemlButton(buttonPanel, "ModifyItem", "수정", 1);
            SetItemlButton(buttonPanel, "DeleteItem", "삭제", 2);

            //itemBox 수정
            this.itemPanel.Dock = DockStyle.Top;
            this.itemPanel.Location = new System.Drawing.Point(0, defaultCardNameHeight);
            this.itemPanel.Size = new System.Drawing.Size(defaultCardWidth, defaultCardHeight - (buttonPanel.Height + cardPanel.Height));
            this.itemPanel.HorizontalScroll.Enabled = false;
            this.itemPanel.VerticalScroll.Enabled = true;
            this.itemPanel.AutoScroll = true;   
            this.itemPanel.BorderStyle = BorderStyle.FixedSingle;

            //크기 재조정
            this.Size = new System.Drawing.Size(defaultCardWidth, defaultCardNameHeight);

            //Drag 이벤트 추가
            cardPanel.MouseDown += new MouseEventHandler(Card_MouseDown);
            cardPanel.MouseUp += new MouseEventHandler(Card_MouseUp);
            cardPanel.MouseMove += new MouseEventHandler(Card_MouseMove);

            this.Controls.Add(buttonPanel);
            this.Controls.Add(itemPanel);
            this.Controls.Add(cardPanel);

        }

        static private String PopupForm(String title, String txt)
        {
            Card card = null;
            Form inputForm = new Form();
            TextBox name = new TextBox();
            Button btn = new Button();

            inputForm.Text = title;
            inputForm.Size = new System.Drawing.Size(190, 80);
            inputForm.ControlBox = false;
            inputForm.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            inputForm.FormBorderStyle = FormBorderStyle.FixedSingle;

            name.Dock = DockStyle.Fill;
            name.BackColor = System.Drawing.Color.LightYellow;
            name.MaxLength = 14;
            name.Text = txt;

            btn.Dock = DockStyle.Bottom;
            btn.Text = "확인";
            btn.Size = new System.Drawing.Size(100, 20);

            btn.MouseClick += SubmitBtnClick;


            inputForm.Controls.Add(name);
            inputForm.Controls.Add(btn);

            inputForm.ShowDialog();

            return name.Text;
        }

        public static Card MakeCard(int num)
        {
            String tmp = Card.PopupForm("카드 생성", "");

            CardData cardData = new CardData();
            cardData.CardIndex = num;
            cardData.CardName = tmp;
            DataSet.Instance.SetCard(cardData);

            return new Card(num, tmp);
        }
        public void ModifyCard()
        {
            String tmp = Card.PopupForm("카드 수정", this.name.Text);
            CardData card = DataSet.Instance.FindCardData(this.name.Text, this.index);
            card.CardName = tmp;
            this.name.Text = tmp;
        }

        private static void SubmitBtnClick(object sender, MouseEventArgs e)
        {
            Form form = (Form)((Button)sender).Parent;
            TextBox data = (TextBox)form.Controls[0];

            if(data.Text.Length == 0)
            {
                System.Windows.Forms.MessageBox.Show("카드 이름을 입력해주세요");
            }
            else ((Form)(((Button)sender).Parent)).Close();
        }

        private void Card_MouseDown(object sender, MouseEventArgs e)
        {
            Console.WriteLine("click");
            Console.WriteLine(GetChildAtPoint(e.Location));
            prePosition = this.Location;
            mousePosition = e.Location;
            mouseClick = true;
        }
        private void Card_MouseUp(object sender, MouseEventArgs e)
        {
            if (mouseClick)
            {
                this.Location = new System.Drawing.Point((this.Location.X + e.X) - mousePosition.X, (this.Location.Y + e.Y) - mousePosition.Y);
                mouseClick = false;
                parentPanel.UIDragLocation(this);
                prePosition = this.Location;
            }
        }

        private void Card_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseClick)
            {
                this.Location = new System.Drawing.Point((this.Location.X + e.X) - mousePosition.X, (this.Location.Y + e.Y) - mousePosition.Y);
            }
        }

        private void MakeItem()
        {
            //item 객체 생성하여 itemList에 삽입됨
            if (itemPanel != null)
            {
                Item item = Item.MakeItem(this);
 
                if (item != null)
                {
                    itemList.Add(item);
                    itemPanel.Controls.Add(item);
                    item.BringToFront();
                }
            }
            //elementPanel.UIRelocate(this);
        }
        private void ChangeItem(Item item)
        {
            //item의 구성 값 변경
            if (item != null)
            {
                item.ModifyItem();
                //elementPanel.UIRelocate(this);
            }
        }
        private void DeleteItem(Item item)
        {
            //item 삭제
            if(item != null)
            {
                CardData cardData = DataSet.Instance.FindCardData(this.name.Text, this.Index);
                ItemData itemData = cardData.FindItemData(cardData.CardIndex, item.Content);
                cardData.DeleteItemData(itemData);

                itemList.Remove(item);
                itemPanel.Controls.Remove(item);
                item.Dispose();
            }
        }

        private void SetItemlButton(Panel buttonPanel, String name, String txt, int num)
        {
            Button button = new Button();
            new ToolTip().SetToolTip(button, "Test");
            button.Dock = DockStyle.Right;
            button.Location = new System.Drawing.Point(defaultCardWidth / 3 * num, 0);
            button.Name = name;
            button.Size = new System.Drawing.Size(defaultCardWidth / 3, defaultButtonHeight);
            button.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            button.ForeColor = System.Drawing.Color.White;
            button.TabIndex = 0;
            button.Text = txt;button.Font = new System.Drawing.Font("나눔고딕", 10);

            //버튼 테두리 없애기
            button.UseVisualStyleBackColor = true;
            button.FlatStyle = FlatStyle.Flat;
            button.FlatAppearance.BorderSize = 0;


            if (num == 0) button.Click += MakeItemButton_Click;
            else if (num == 1) button.Click += ModifyButton_Click;
            else button.Click += DeleteButton_Click;

            buttonPanel.Controls.Add(button);
        }

        private void MakeItemButton_Click(object sender, EventArgs e)
        {
            MakeItem();
        }
        private void ModifyButton_Click(object sender, EventArgs e)
        {
            Item item = FindOnlyCheckedItem();
            ChangeItem(item);
        }
        private void DeleteButton_Click(object sender, EventArgs e)
        {
            Item item = FindOnlyCheckedItem();
            if (item != null && (MessageBox.Show("정말 Item을 삭제하시겠습니까?", "Item 삭제", MessageBoxButtons.YesNo)) == DialogResult.Yes)
            {
                DeleteItem(item);
            }
        }

        private Item FindOnlyCheckedItem()
        {
            Item item;
            Item check = null;

            for (int i = 0; i < itemList.Count; i++)
            {
                item = itemList[i];
                if (item.Checked)
                {
                    //한개 이상 체크되어 있으면 무시
                    if (check == null) check = item;
                    else
                    {
                        System.Windows.Forms.MessageBox.Show("Item을 하나만 선택해주세요");
                        return null;
                    }
                }
            }

            if (check == null) System.Windows.Forms.MessageBox.Show("Item을 선택해주세요");

            return check;
        }

        private void CardName_CheckedChanged(object sender, EventArgs e)
        {
            this.SuspendLayout();

            if (check.Checked) this.Size = new System.Drawing.Size(defaultCardWidth, defaultCardHeight);
            else this.Size = new System.Drawing.Size(defaultCardWidth, defaultCardNameHeight);

            this.ResumeLayout(false);
        }

        public bool Checked
        { get { return check.Checked; } }
        public Point PrePosition
        {
            get { return prePosition; }
            set { prePosition = value; }
        }
        public static int cardWidth
        { get { return defaultCardWidth; } }
        public static int cardHeight
        { get { return defaultCardHeight; } }
        public static int cardNameHeight
        { get { return defaultCardNameHeight; } }
        public static int buttonHeight
        { get { return defaultButtonHeight; } }
        public ElementPanel parentPanel
        {
            get { return elementPanel; }
            set { elementPanel = value; }
        }
        public string CardName
        {
            get { return name.Text; }
        }
        public List<Item> GetItems
        { get { return itemList; } }
        public Panel GetitemPanel
        { get { return itemPanel; } }
        public int Index
        {
            get { return index; }
            set { index = value; }
        }
    }
}
