﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortfolioManagement
{
    [Serializable]
    public class CardData
    {
        private string cardName;
        private int cardIndex;
        private List<ItemData> items;

        public CardData()
        {
            items = new List<ItemData>();
        }

        public void AddItem(ItemData data)
        {
            items.Add(data);
        }

        public List<ItemData> ItemList { get { return items; } }

        public ItemData FindItemData(int index, string contents)
        {
            foreach (ItemData item in items)
            {
                if ((item.ParentCardIndex == index) && (item.ItemContent == contents))
                    return item;
            }
            return null;
        }

        public void DeleteItemData(ItemData item)
        {
            items.Remove(FindItemData(item.ParentCardIndex, item.ItemContent));
        }

        public void SetCardIndex(int index)
        {
            cardIndex = index;
            foreach(ItemData item in items)
            {
                item.ParentCardIndex = index;
            }
        }

        public string CardName { get { return cardName; } set { cardName = value; } }
        public int CardIndex { get { return cardIndex; } set { cardIndex = value; } }

    }
}
