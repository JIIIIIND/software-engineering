﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortfolioManagement
{
    [Serializable]
    class DataSet
    {
        private static DataSet dataSet;
        private List<CardData> cardList;
        
        private string filePath;

        private DataSet()
        {
            cardList = new List<CardData>();
        }

        public void Initialize()
        {
            dataSet = null;
            cardList = null;
        }

        public void SetCard(CardData card)
        {
            card.CardIndex = cardList.Count;
            cardList.Add(card);
        }

        public List<CardData> CardList
        { get { return cardList; } }

        public CardData FindCardData(string name, int index)
        {
            foreach (CardData card in cardList)
            {
                if ((card.CardName == name) && (card.CardIndex == index))
                {
                    return card;
                }
            }
            return null;
        }

        public void DeleteCardData(CardData card)
        {
            cardList.Remove(FindCardData(card.CardName, card.CardIndex));
        }
        public string FilePath { get { return filePath; } set { filePath = value; } }

        public static DataSet Instance
        {
            get
            {
                if (dataSet != null)
                    return dataSet;
                else
                {
                    dataSet = new DataSet();
                    return dataSet;
                }
            }
            set
            {
                dataSet = value;
            }
        }
    }
}
