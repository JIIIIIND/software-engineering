﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PortfolioManagement
{
    class ElementPanel : Panel
    {
        //생성된 카드를 저장함, 카드 내부에 각각 아이템을 보관하는 패널로 불러질 수 있음
        private List<Card> elementList;
        private Card activeCard;
        private System.Drawing.Point prePosition;
        private System.Drawing.Point curPositon;

        public ElementPanel()
        {
            elementList = new List<Card>();

            this.Dock = DockStyle.Fill;

            this.BackColor = System.Drawing.Color.White;
            this.BorderStyle = BorderStyle.FixedSingle;
            this.Name = "ElementPanel";
            this.TabIndex = 0;

            //스크롤 설정
            this.AutoScroll = true;
            this.VScroll = false;

        }

        private int GetLocation(System.Drawing.Point cardLocation)
        {
            //가까운 위치 찾기
            for (int i = 0; i < elementList.Count; i++)
            {
                if (cardLocation.X < (10 + (Card.cardWidth + 10) * i + Card.cardWidth))
                {
                    return (10 + (Card.cardWidth + 10) * i);
                }
            }
            return (10 + (Card.cardWidth + 10) * (elementList.Count -1));
        }

        private int GetCardIndex(int location, Card card)
        {
            int preGap = int.MaxValue;
            int index = elementList.Count - 1;
            for (int i = 0; i < elementList.Count; i++)
            {
                if (card != elementList[i])
                {
                    if (location < (elementList[i].Location.X + Card.cardWidth))
                    {
                        int newGap = (elementList[i].Location.X + Card.cardWidth) - location;
                        if (newGap < 0) newGap = -newGap;
                        if (newGap < preGap)
                        {
                            preGap = newGap;
                            index = i;
                        }
                    }
                }
                else if(location == card.PrePosition.X)
                {
                    return i;
                }
            }
            return index;
        }
        private void SetCardLocation(int location, Card card)
        {
            //카드 위치 이동
            if (card != null)
            {
                card.Location = new System.Drawing.Point(location, 10);
            }
        }

        public void LocationInit()
        {
            CardData cardData;
            for (int i = 0; i < elementList.Count; i++)
            {
                int startPosition = 10;
                int newGap = int.MaxValue;
                int initIndex = 0;
                for (int index = 0; index < elementList.Count; index++)
                {
                    int gap = elementList[i].Location.X - startPosition;
                    if (gap < 0) gap = -gap;

                    if (newGap > gap)
                    {
                        newGap = gap;
                        initIndex = index;
                    }
                    startPosition += (Card.cardWidth + 10);
                }
                elementList[i].Location = new System.Drawing.Point(10 + (Card.cardWidth + 10) * initIndex, 10);

                cardData = DataSet.Instance.FindCardData(elementList[i].CardName, elementList[i].Index);
                cardData.SetCardIndex(initIndex);
                elementList[i].Index = initIndex;
            }
        }

        public void UIRelocate(List<Card> cards)
        {
            for (int i = 0; i < cards.Count; i++)
            {
                int position = 10 + (Card.cardWidth + 10) * i;
                Card selectCard = null;
                foreach (Card card in cards)
                {
                    if (card.Location.X >= position)
                    {
                        if (selectCard == null)
                            selectCard = card;
                        else if ((selectCard.Location.X - position) > (card.Location.X - position))
                            selectCard = card;
                    }
                }
                SetCardLocation(position, selectCard);
                selectCard.PrePosition = new System.Drawing.Point(position, 10);
            }
        }

        public void UIDragLocation(Card card)
        {
            //elementList 노드들의 우선순위를 파악하여 재조정
            this.SuspendLayout();

            int preLocation = GetLocation(card.PrePosition);
            //선택된 카드의 이동 위치
            int changeLocation = GetLocation(card.Location);
            //이동할 위치의 카드 인덱스
            int targetCardIndex = GetCardIndex(changeLocation, card);

            SetCardLocation(preLocation, elementList[targetCardIndex]);
            SetCardLocation(changeLocation, card);
            
            LocationInit();
            this.ResumeLayout(false);
        }
        
        public void InitCardList()
        {
            foreach (Card card in elementList)
            {
                card.Dispose();
            }
            elementList.Clear();
        }

        public List<Card> list
        {
            get { return elementList; }
        }
        public void AddElement(Card card)
        {
            elementList.Add(card);
            PortfolioManager.GetManager.Cards = elementList;
        }
    }
}
