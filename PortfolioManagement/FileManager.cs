﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;
using System.ComponentModel;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;

namespace PortfolioManagement
{
    class FileManager
    {
        private float autoSaveTime;

        public bool loadSuccess = false;
        
        private BackgroundWorker worker;
        
        public FileManager()
        {
            autoSaveTime = 6000;
            worker = new BackgroundWorker();
            worker.RunWorkerAsync();
            worker.DoWork += new DoWorkEventHandler(AutoSave);//AutoSave메소드 실행
            //초기 값 설정
        }
        
        [Serializable]
        class saveObject
        {
            //public CardData cardData = CardData.Instance;
            public DataSet dataSet= DataSet.Instance;
            //public ItemData itemData = ItemData.Instance;
        }
        //Portfolio Manager의 직렬화 데이터를 path에 저장
        public void Save(bool auto)
        {
            object saveData = new saveObject();
            saveObject saveobj;
            saveobj = (saveObject)saveData;
            //Console.WriteLine(DataSet.Instance.CardList);
            //Console.WriteLine(saveobj.dataSet);

            // 받을 객체 정의
            //System.Console.WriteLine(pfm);
            BinaryFormatter bf = new BinaryFormatter();

            //XmlSerializer ser = new XmlSerializer(typeof(PortfolioManager));
            //TextWriter writer = new StreamWriter(pfm.Path);
            //ser.Serialize(writer, pfm);
            if (!auto)
            {
                using (FileStream fs = new FileStream(PortfolioManager.GetManager.Path, FileMode.Create))
                {
                    bf.Serialize(fs, saveobj);
                    fs.Close();
                }
            }
            else
            {
                using (FileStream fs = new FileStream(PortfolioManager.GetManager.Path + "(clone)", FileMode.Create))
                {
                    bf.Serialize(fs, saveobj);
                    fs.Close();
                }
            }
        }

        //path로부터 불러온 직렬화 데이터를 Portfolio Manager에 삽입
        public void Load()
        {
            object saveData = new saveObject();
            saveObject saveobj;
            saveobj = (saveObject)saveData;
            // 저장할 객체
            BinaryFormatter bf = new BinaryFormatter();

            if ((PortfolioManager.GetManager.Path != null)&&(PortfolioManager.GetManager.Path != ""))
            {
                using (FileStream fs = new FileStream(PortfolioManager.GetManager.Path, FileMode.Open))
                {
                    saveobj = (saveObject)bf.Deserialize(fs);
                    fs.Close();
                }
                //CardData.Instance = saveobj.cardData;
                DataSet.Instance.Initialize();
                DataSet.Instance = saveobj.dataSet;
                //ItemData.Instance = saveobj.itemData;
                loadSuccess = true;
                PortfolioManager.GetManager.MainUI.UILoad();
            }
        }
        
        private void AutoSave(object obj, DoWorkEventArgs e) //Save함수 계속 실행
        {
            while (true)
            {   
                if(PortfolioManager.GetManager.Path != null) { 
                    Thread.Sleep((int)autoSaveTime); //1 분
                    try
                    {
                        Console.WriteLine("Auto Save Complete");
                        Save(true);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                    }
                }
            }
        }
    }
}
