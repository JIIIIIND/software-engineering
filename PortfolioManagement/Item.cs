﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PortfolioManagement
{
    public enum DataType
    {STRING, INTEEGER, FLOAT, GRID}
    
    class Item : Panel
    {
        private Card parentCard;
        private CheckBox checkButton;
        private TextBox contents;
        private DataType type;
        private Memo memo;

        private int defaultContextWidth;
        private int defaultItemHeight = 25;
        private int defaultItemMaxHeight;
       
        public Item(ItemData itemData, Card parent)
        {
            InitializeComponent(itemData.ItemContent, itemData.Type, itemData.ParentWidth);
            this.parentCard = parent;
        }
        private Item(String txt, DataType dataType, int Width)
        {
            InitializeComponent(txt, dataType, Width);
        }

        private void InitializeComponent(String txt, DataType dataType, int Width)
        {
            this.contents = new TextBox();
            this.contents.Text = txt;
            this.type = dataType;

            //체크박스 설정
            checkButton = new CheckBox();
            checkButton.Size = new System.Drawing.Size(13, 13);
            checkButton.Location = new System.Drawing.Point(5, 3);
            checkButton.CheckedChanged += ItemCheckedChanged;

            //contents 설정
            defaultContextWidth = Width - 45;

            contents.ReadOnly = true;
            contents.Multiline = true;
            contents.WordWrap = true;
            contents.Enabled = false;
            contents.BackColor = System.Drawing.Color.White;
            contents.Size = new System.Drawing.Size(defaultContextWidth, contents.Font.Height + 4);
            contents.Location = new System.Drawing.Point(checkButton.Width + 10, 3);
            contents.BorderStyle = BorderStyle.FixedSingle;

            defaultItemMaxHeight = contents.PreferredHeight + 10;

            //UI 설정
            this.Dock = DockStyle.Top;
            this.Size = new System.Drawing.Size(Width, defaultItemHeight);
            this.BorderStyle = BorderStyle.FixedSingle;

            defaultItemHeight = this.Height;

            this.Controls.Add(checkButton);
            this.Controls.Add(contents);

            memo = new Memo(this);
            this.ContextMenu = memo.GetContextMenu();
        }

        //팝업 호출을 통해 생성
        static private Item PopupForm(String formName, String txt, int Width)
        {
            Item item = null;
            Form inputForm = new Form();
            ComboBox type = new ComboBox();
            TextBox text = new TextBox();
            Button btn = new Button();

            inputForm.Text = formName;
            inputForm.Size = new System.Drawing.Size(150, 200);
            inputForm.ControlBox = false;
            inputForm.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;

            type.Dock = DockStyle.Top;
            type.Items.Add("문자");
            type.Items.Add("숫자");
            type.DropDownStyle = ComboBoxStyle.DropDownList;
            type.SelectedIndex = 0;

            text.Dock = DockStyle.Fill;
            text.BackColor = System.Drawing.Color.LightYellow;
            text.Text = txt;
            text.Multiline = true;
            text.WordWrap = true;

            btn.Dock = DockStyle.Bottom;
            btn.Text = "확인";
            btn.Size = new System.Drawing.Size(100, 20);

            btn.MouseClick += SubmitBtnClick;


            inputForm.Controls.Add(text);
            inputForm.Controls.Add(type);
            inputForm.Controls.Add(btn);

            inputForm.ShowDialog();

            if ((String)type.SelectedItem == "문자")
            {
                item = new Item(text.Text, DataType.STRING, Width);
            }
            else
            {
                item = new Item(text.Text, DataType.INTEEGER, Width);
            }
            

            return item;
        }

        static public Item MakeItem(Card parent)
        {
            Item item = Item.PopupForm("아이템 생성창", "", parent.Width);
            
            ItemData itemData = new ItemData(DataSet.Instance.FindCardData(parent.CardName, parent.Index).CardIndex);
            itemData.ItemContent = item.contents.Text;
            itemData.ParentWidth = parent.Width;
            itemData.Type = item.type;
            DataSet.Instance.FindCardData(parent.CardName, parent.Index).AddItem(itemData);

            item.parentCard = parent;

            return item;
        }

        public void ModifyItem()
        {
            Item tmp = Item.PopupForm("아이템 수정창", contents.Text.ToString(), this.parentCard.Width);

            ItemData item = 
                DataSet.Instance.FindCardData(parentCard.CardName, parentCard.Index).
                FindItemData(DataSet.Instance.FindCardData(parentCard.CardName, parentCard.Index).CardIndex, this.contents.Text);
            item.Type = tmp.type;
            item.ItemContent = tmp.contents.Text;

            this.type = tmp.type;
            this.contents.Text = tmp.contents.Text;

            tmp.Dispose();

            //Item 크기 재조정
            this.contents.Height = this.contents.Font.Height + 4;

            this.contents.Height += this.contents.GetPositionFromCharIndex(this.contents.Text.Length - 1).Y + 3
                    + contents.Font.Height - contents.ClientSize.Height;

            this.Height = this.defaultItemHeight + this.contents.Height - this.contents.Font.Height - 5;
        }

        private static void SubmitBtnClick(object sender, MouseEventArgs e)
        {
            Form form = (Form)((Button)sender).Parent;
            String type = (String)((ComboBox)form.Controls[1]).SelectedItem;
            TextBox data = (TextBox)form.Controls[0];

            if (type == "숫자")
            {
                //문자열 제거
                data.Text = System.Text.RegularExpressions.Regex.Replace(data.Text, @"\D", "");

            }

            if(data.Text.Length == 0)
            {
                System.Windows.Forms.MessageBox.Show("타입과 일치하는 데이터가 존재하지 않습니다");
            }
            else ((Form)(((Button)sender).Parent)).Close();
        }

        private void ItemCheckedChanged(object sender, EventArgs e)
        {
            this.SuspendLayout();

            if (this.Height < defaultItemMaxHeight)
            {
                contents.Height += contents.GetPositionFromCharIndex(contents.Text.Length - 1).Y + 3
                    + contents.Font.Height - contents.ClientSize.Height;
                this.Height += contents.Height - contents.Font.Height - 5;
            }
            else
            {
                contents.Height = contents.Font.Height + 4;
                this.Height = defaultItemHeight;
            }


            this.ResumeLayout(false);
        }


        public string Content
        {
            get
            {
                return contents.Text;
            }
        }

        public Point prePosition
        {
            get { return prePosition; }
        }

        public bool Checked
        {
            get { return checkButton.Checked; }
        }
    }
}
