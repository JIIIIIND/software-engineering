﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortfolioManagement
{
    [Serializable]
    public class ItemData
    {
        private string itemContent;
        private DataType type;
        private int parentCardIndex;
        private int parentWidth;

        public ItemData(int parentIndex)
        {
            parentCardIndex = parentIndex;
        }
        
        public DataType Type { get { return type; } set { type = value; } }
        public string ItemContent { get { return itemContent; } set { itemContent = value; } }
        public int ParentCardIndex { get { return parentCardIndex; } set { parentCardIndex = value; } }
        public int ParentWidth { get { return parentWidth; } set { parentWidth = value; } }

    }

    
}
