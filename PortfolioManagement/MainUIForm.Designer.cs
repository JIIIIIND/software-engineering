﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
namespace PortfolioManagement
{
    partial class MainUIForm
    {
        private ElementPanel elementPanel;
        //elementPanel은 Card들이 배치되는 Panel
        private Panel mainPanel;
        //파일 저장, 로드 , 새 파일 등등 시스템적인 버튼이 놓일 Panel

        private static int defaultButtonHeight = 25;
        private static int defaultButtonPadSize = 5;
        private static int defaultButtonImageX = 5;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainUIForm));

            this.mainPanel = new System.Windows.Forms.Panel();
            this.elementPanel = new ElementPanel();
            this.FormBorderStyle = FormBorderStyle.FixedToolWindow;
            this.Icon = (System.Drawing.Icon)(resources.GetObject("portfolio"));

            this.SuspendLayout();

            SetMainPanel(mainPanel, resources);

            // MainUIForm
            this.Name = "MainUIForm";
            this.Text = "Portfolio Manager";

            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.BackColor = System.Drawing.Color.White;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1210, 470);

            this.Controls.Add(this.elementPanel);
            this.Controls.Add(GetBanner(mainPanel.Width));
            this.Controls.Add(this.mainPanel);

            this.ResumeLayout(false);

        }

        private void SetMainPanel(Panel panel, System.ComponentModel.ComponentResourceManager resources)
        {
            PictureBox picture = new PictureBox();

            panel.Dock = System.Windows.Forms.DockStyle.Left;
            panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            panel.Location = new System.Drawing.Point(0, 0);

            panel.Name = "mainPanel";
            panel.Size = new System.Drawing.Size(150, this.Height);
            panel.TabIndex = 0;

            //이미지 세팅
        
            picture.Image = ((System.Drawing.Image)(resources.GetObject("gear")));
            picture.SizeMode = PictureBoxSizeMode.StretchImage;
            picture.Size =  new System.Drawing.Size(100,100);
            picture.Location = new System.Drawing.Point((mainPanel.Width - picture.Width)/2, 10);
            
            panel.Controls.Add(picture);

            //panel Button 세팅
            SetMainPanelButton(panel, "NewData", "새로만들기", 3, GetImage(resources, "new"));
            SetMainPanelButton(panel, "Load", "불러오기", 4, GetImage(resources, "load"));
            SetMainPanelButton(panel, "Save", "저장하기", 5, GetImage(resources, "save"));

            SetMainPanelButton(panel, "MakeCard", "카드 생성", 7, GetImage(resources, "makeCard"));
            SetMainPanelButton(panel, "ModifyCard", "카드 수정", 8, GetImage(resources, "modifyCard"));
            SetMainPanelButton(panel, "DeleteCard", "카드 삭제", 9, GetImage(resources, "deleteCard"));

            SetMainPanelButton(panel, "MakeReport", "리포트 생성", 11, GetImage(resources, "new"));

            SetButtonEnable(false);
        }

        private Panel GetBanner(int mainPanelWidth)
        {
            Panel banner = new Panel();

            Console.Out.WriteLine(mainPanelWidth);

            banner.Dock = System.Windows.Forms.DockStyle.Top;
            banner.BackColor = System.Drawing.SystemColors.Control;
            banner.Location = new System.Drawing.Point(mainPanelWidth, 0);
            banner.Size = new System.Drawing.Size(this.Width - mainPanelWidth, 80);
            banner.TabIndex = 0;

            Label bannerContents = new Label();
            bannerContents.AutoSize = true;
            bannerContents.Font = new System.Drawing.Font("휴먼굵은팸체", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            bannerContents.Location = new System.Drawing.Point(6, (banner.Height - bannerContents.Height) / 2);
            bannerContents.Name = "BannerContents";
            bannerContents.Size = new System.Drawing.Size(262, 40);
            bannerContents.TabIndex = 1;
            bannerContents.Text = "Portfolio Manager";
            bannerContents.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;

            banner.Controls.Add(bannerContents);

            return banner;
        }

        private void SetMainPanelButton(Panel panel, string name, string txt, int num, Image image)
        {
            Button button = new Button();
            PictureBox picture = new PictureBox();

            button.Location = new System.Drawing.Point(defaultButtonImageX + defaultButtonPadSize, 10 + (10 + defaultButtonHeight) * num);
            button.Name = name;
            button.Size = new System.Drawing.Size(panel.Width - defaultButtonPadSize*2 - (defaultButtonImageX + 10), defaultButtonHeight);
            button.TabIndex = 0;
            button.Text = txt;

            //버튼 테두리 없애기
            button.UseVisualStyleBackColor = true;
            button.FlatStyle = FlatStyle.Flat;
            button.FlatAppearance.BorderSize = 0;

            //글꼴 설정
            button.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(66)))));
            button.ForeColor = System.Drawing.Color.White;
            button.Font = new System.Drawing.Font("나눔고딕", 10);

            //이미지 세팅
            button.Image = new System.Drawing.Bitmap(image, new System.Drawing.Size(15, 15));
            button.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;

            //이벤트 설정
            if (name == "NewData") button.Click += NewFileButton_Click;
            else if (name == "Save") button.Click += SaveFileButton_Click;
            else if (name == "Load") button.Click += LoadFileButton_Click;

            else if (name == "MakeCard") button.Click += MakeCardButton_Click;
            else if (name == "ModifyCard") button.Click += ModifyCardButton_Click;
            else if (name == "DeleteCard") button.Click += DeleteCardButton_Click;

            else if (name == "MakeReport") button.Click += MakeReportButton_Click;

            panel.Controls.Add(button);
        }

        private void NewFileButton_Click(object sender, EventArgs e)
        {
            elementPanel.InitCardList();
            DataSet.Instance.Initialize();
            SetButtonEnable(true);
        }

        private void SaveFileButton_Click(object sender, EventArgs e)
        {
            elementPanel.LocationInit();

            SaveFileDialog tmp = new SaveFileDialog();
            SetPathManager(tmp, "저장하기");
            PortfolioManager.GetManager.FileSave();

        }
        private void LoadFileButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog tmp = new OpenFileDialog();
            SetPathManager(tmp, "불러오기");

            PortfolioManager.GetManager.FileLoad();

            //결과에 따라 버튼 활성화 선택
            if (PortfolioManager.GetManager.fileManager.loadSuccess) SetButtonEnable(true);
        }

        private void MakeCardButton_Click(object sender, EventArgs e)
        {
            PortfolioManager.GetManager.MakeCard(elementPanel);
        }
        private void ModifyCardButton_Click(object sender, EventArgs e)
        {
            List<Card> elementList = elementPanel.list;
            Card card = FindOnlyCheckedCard(elementList);
            PortfolioManager.GetManager.ModifyCard(elementPanel, card);
        }
        private void DeleteCardButton_Click(object sender, EventArgs e)
        {
            List<Card> elementList = elementPanel.list;
            Card card = FindOnlyCheckedCard(elementList);
            String warning = "정말 선택한 카드를 삭제하시겠습니까?\n" +
                             "※ 포함한 Item들도 모두 사라집니다.";

            if (card != null && (MessageBox.Show(warning, "카드 삭제", MessageBoxButtons.YesNo) == DialogResult.Yes))
            {
                PortfolioManager.GetManager.DeleteCard(elementPanel, card);
            }
        }

        private void MakeReportButton_Click(object sender, EventArgs e)
        {
            PortfolioManager.GetManager.MakeReport();
        }

        private void SetPathManager(FileDialog tmp, String name)
        {
            tmp.Title = name;
            String tmpPath = PortfolioManager.GetManager.Path;

            if (tmpPath == "") tmp.InitialDirectory = ".\\";
            else tmp.InitialDirectory = tmpPath;
            tmp.Filter = ".dat File|*.dat";

            tmp.ShowDialog();

            PortfolioManager.GetManager.Path = tmp.FileName.ToString();

            tmp.Dispose();
        }

        private void SetButtonEnable(bool enable)
        {
            for (int i = 3; i < mainPanel.Controls.Count; i++) mainPanel.Controls[i].Enabled = enable;
        }

        private Card FindOnlyCheckedCard(List<Card> elementList)
        {
            Card card;
            Card check = null;

            for (int i = 0; i < elementList.Count; i++)
            {
                card = elementList[i];
                if (card.Checked)
                {
                    //한개 이상 체크되어 있으면 무시
                    if (check == null) check = card;
                    else
                    {
                        System.Windows.Forms.MessageBox.Show("Card를 하나만 선택해주세요");
                        return null;
                    }
                }
            }

            if (check == null) System.Windows.Forms.MessageBox.Show("카드를 선택해주세요");

            return check;
        }

        private Image GetImage(System.ComponentModel.ComponentResourceManager resources, String name)
        {
            return ((System.Drawing.Icon)(resources.GetObject(name))).ToBitmap();
        }
        #endregion

        public void UILoad()
        {
            List<CardData> cardDataList = DataSet.Instance.CardList;
            List<ItemData> itemDataList;

            Card card;
            Item item;

            elementPanel.InitCardList();

            foreach (CardData cardData in cardDataList)
            {
                card = new Card(cardData);
                card.parentPanel = elementPanel;
                elementPanel.AddElement(card);

                itemDataList = cardData.ItemList;

                foreach (ItemData itemData in itemDataList)
                {
                    item = new Item(itemData, card);
                    card.GetItems.Add(item);
                    card.GetitemPanel.Controls.Add(item);
                    item.BringToFront();
                }

                elementPanel.Controls.Add(card);
            }
        }
    }
}