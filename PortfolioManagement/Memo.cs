﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PortfolioManagement
{
    class Memo
    {
        Control parnets;
        String memo;

        public Memo(Control element)
        {
            //Memo Control 생성
            parnets = element;
            memo = "";
        }

        public ContextMenu GetContextMenu()
        {
            ContextMenu menu = new ContextMenu();

            MenuItem makeMemo = new MenuItem();
            MenuItem deleteMemo = new MenuItem();

            makeMemo.Text = "메모 보기";
            deleteMemo.Text = "메모 삭제";

            makeMemo.Click += MakeButton_Click;
            deleteMemo.Click += DeleteButton_Click;

            menu.MenuItems.Add(makeMemo);
            menu.MenuItems.Add(deleteMemo);

            return menu;
        }

        private void MakeMemoForm()
        {
            Form memoForm = new Form();
            TextBox contents = new TextBox();
            Button btn = new Button();

            memoForm.Text = parnets.Text + " 메모장";
            memoForm.Size = new System.Drawing.Size(100, 120);
            memoForm.FormBorderStyle = FormBorderStyle.SizableToolWindow;
            memoForm.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;

            contents.Dock = DockStyle.Fill;
            contents.BackColor = System.Drawing.Color.LightYellow;
            contents.Text = memo;
            contents.Multiline = true;

            btn.Dock = DockStyle.Bottom;
            btn.Text = "확인";
            btn.Size = new System.Drawing.Size(100, 20);

            btn.MouseClick += SubmitBtnClick;

            memoForm.Controls.Add(contents);
            memoForm.Controls.Add(btn);
            memoForm.Show();
        }

        private void SubmitBtnClick(object sender, MouseEventArgs e)
        {
            Form tmp = ((Form)(((Button)sender).Parent));
            memo = ((TextBox)tmp.Controls[0]).Text;

            tmp.Close();
        }

        private void MakeButton_Click(object sender, System.EventArgs e)
        {
            MakeMemoForm();
        }

        private void DeleteButton_Click(object sender, System.EventArgs e)
        {
            this.memo = "";
        }
    }
}
