﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;

namespace PortfolioManagement
{
    class PortfolioManager
    {
        private static PortfolioManager manager;
        private MainUIForm mainUI;
        public FileManager fileManager;
        private string filePath;
        private List<Card> cards;

        public PortfolioManager()
        {
            mainUI = new MainUIForm();
            fileManager = new FileManager();
        }
        /// <summary>
        /// 해당 응용 프로그램의 주 진입점입니다.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            manager = new PortfolioManager();
            Application.Run(manager.mainUI);
        }

        public void MakeCard(ElementPanel elementPanel)
        {
            Card card = Card.MakeCard(elementPanel.Controls.Count);

            if (card != null)
            {
                elementPanel.AddElement(card);
                elementPanel.Controls.Add(card);
                card.parentPanel = elementPanel;

                elementPanel.UIRelocate(elementPanel.list);
            }
        }

        public void ModifyCard(ElementPanel elementPanel, Card card)
        {
            if(card != null)
            {
                card.ModifyCard();
                elementPanel.UIRelocate(elementPanel.list);
            }
        }

        public void DeleteCard(ElementPanel elementPanel, Card card)
        {
            if(card != null)
            {
                elementPanel.Controls.Remove(card);
                elementPanel.list.Remove(card);
                DataSet.Instance.DeleteCardData(DataSet.Instance.FindCardData(card.CardName, card.Index));

                card.Dispose();

                elementPanel.UIRelocate(elementPanel.list);
            }
        }

        public void MakeReport()
        {
            Report report = new Report();
            
            report.InitReport();
            report.ShowDialog();
        }
        public void FileSave()
        {
            fileManager.Save(false);
            //FileManager의 Save 함수 호출
        }
        public void FileLoad()
        {
            fileManager.Load();
            //FileManager의 Load 함수 호출

        }
        public List<Card> Cards
        {
            get { return cards; }
            set { cards = value; }
        }

        public static PortfolioManager GetManager
        {
            get { return manager; }
        }

        public string Path
        {
            get { return filePath; }
            set
            {
                filePath = value;
                DataSet.Instance.FilePath = filePath;
            }
        }
        public MainUIForm MainUI
        { get { return mainUI; } }
    }
}
