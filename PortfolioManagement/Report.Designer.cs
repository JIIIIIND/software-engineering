﻿using System;
using System.Collections.Generic;
using System.IO;

namespace PortfolioManagement
{
    partial class Report
    {
        private List<Card> cardList;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        public void GetCheck()
        {
            //리포트를 구성 하기 위해 선택된 Card 값을 가져옴
        }
        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Title = new System.Windows.Forms.Label();
            this.MainPanel = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // Title
            // 
            this.Title.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Title.Dock = System.Windows.Forms.DockStyle.Top;
            this.Title.Font = new System.Drawing.Font("굴림", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Title.Location = new System.Drawing.Point(0, 0);
            this.Title.Name = "Title";
            this.Title.Size = new System.Drawing.Size(584, 100);
            this.Title.TabIndex = 0;
            this.Title.Text = Path.GetFileNameWithoutExtension(PortfolioManager.GetManager.Path);
            this.Title.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // MainPanel
            // 
            this.MainPanel.AutoScroll = true;
            this.MainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainPanel.Location = new System.Drawing.Point(0, 100);
            this.MainPanel.Margin = new System.Windows.Forms.Padding(10);
            this.MainPanel.Name = "MainPanel";
            this.MainPanel.Size = new System.Drawing.Size(584, 661);
            this.MainPanel.TabIndex = 1;
            // 
            // Report
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(584, 761);
            this.Controls.Add(this.MainPanel);
            this.Controls.Add(this.Title);
            this.Name = "Report";
            this.Text = "Report";
            this.ResumeLayout(false);
            
        }

        #endregion

        public void InitReport()
        {
            SuspendLayout();
            cardList = new List<Card>();
            System.Windows.Forms.Panel panel = null;
            int locationY = 10;

            foreach (Card card in PortfolioManager.GetManager.Cards)
            {
                if (card.Checked)
                    cardList.Add(card);
            }
            foreach (Card card in cardList)
            {
                if (panel != null)
                {
                    locationY += panel.Size.Height + 10;
                }
                panel = MakeContentsPanel(card,locationY);
                
                this.MainPanel.Controls.Add(panel);
            }
            ResumeLayout(false);
        }

        private System.Windows.Forms.Panel MakeContentsPanel(Card card, int locationY)
        {
            int yPosition = 0;
            System.Windows.Forms.Panel contentsPanel = new System.Windows.Forms.Panel();
            //
            contentsPanel.AutoScroll = true;
            contentsPanel.AutoSize = true;
            contentsPanel.Location = new System.Drawing.Point(10, locationY);
            contentsPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            contentsPanel.Name = "contentsPanel";
            contentsPanel.Size = new System.Drawing.Size(510, 300);
            contentsPanel.TabIndex = 0;
            //
            System.Windows.Forms.Label cardContents = MakeCardContents(card.CardName, new System.Drawing.Point(10,10), contentsPanel);
            yPosition = 10 + cardContents.Size.Height + cardContents.Location.Y;
            contentsPanel.Controls.Add(cardContents);
            List<Item> items = new List<Item>();
            foreach(Item item in card.GetItems)
            {
                if (item.Checked)
                    items.Add(item);
            }
            for (int itemIndex = 0; itemIndex < items.Count; itemIndex++)
            {
                System.Windows.Forms.Label itemContent = MakeItemContents(items[itemIndex].Content, new System.Drawing.Point(10,yPosition), contentsPanel);
                //yPosition += 10 + itemContent.Size.Height + itemContent.Location.Y;
                yPosition += 10 + itemContent.Size.Height;
                contentsPanel.Controls.Add(itemContent);
            }
            return contentsPanel;
        }
        private System.Windows.Forms.Label MakeCardContents(string cardName, System.Drawing.Point position, System.Windows.Forms.Panel panel)
        {
            System.Windows.Forms.Label CardContents = new System.Windows.Forms.Label();

            //
            CardContents.Font = new System.Drawing.Font("굴림", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            CardContents.Location = position;
            CardContents.Name = cardName;
            CardContents.Size = new System.Drawing.Size(500, 50);
            CardContents.TabIndex = 0;
            CardContents.Text = cardName;
            CardContents.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            //

            return CardContents;
        }
        private System.Windows.Forms.Label MakeItemContents(string itemContent, System.Drawing.Point position, System.Windows.Forms.Panel panel)
        {
            System.Windows.Forms.Label itemField = new System.Windows.Forms.Label();

            itemField.AutoSize = true;
            itemField.MaximumSize = new System.Drawing.Size(500,int.MaxValue);
            itemField.Location = position;
            itemField.Name = itemContent;
            //사이즈는 변경하는 함수 적용
            //itemField.Size = new System.Drawing.Size(500, 100);
            itemField.Margin = new System.Windows.Forms.Padding(10);
            itemField.Text = itemContent;
            itemField.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            System.Drawing.Graphics g = System.Drawing.Graphics.FromHwnd(this.Handle);
            System.Drawing.SizeF size = g.MeasureString(itemContent, itemField.Font,500);
            itemField.Size = new System.Drawing.Size(500, (int)size.Height + 10);

            return itemField;
        }

        private System.Windows.Forms.Label Title;
        private System.Windows.Forms.Panel MainPanel;
    }
}